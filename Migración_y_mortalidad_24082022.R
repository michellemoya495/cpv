#--------------------------PROPUESTA CÁLCULO DE INDICADORES-------------------------------------
#------------------------------MORTALIDAD Y MIGRACION-----------------------------------------------
#Cargar librerias

require( data.table )
require( dplyr )
require( foreign )
require(stringr)
require(tidyr)
require(expss)
require(labelled)
require(writexl)
require(magrittr)
require(openxlsx)
require(janitor)
require(readxl)
require(ggplot2)
require(readstata13)
require(haven)
require(scales)
require(hrbrthemes)
require(GGally)
require(viridis)


install.packages("highcharter")
library(plyr)
library(dplyr)
library(forcats)
library(glue)
library(waffle)
library(sjlabelled)
library(stringr)
library(highcharter)
library(tidyverse)

 #Setear directorio

setwd("D:/Michelle/04_Censo Piloto/Bases Abril 2021")

#Cargar bases

all.bdds = list.files(pattern = ".txt")

fromTXT = function(x){
  tmp = fread (file = x) 
  tmp=as.data.table(tmp)
  out = list(tmp)
  return(out)
}

for (x in all.bdds){
  n0 = gsub(".txt", "", x)
  tmp = fromTXT(x)
  assign(n0,tmp[[1]]) 
} 

#Leer bases de data Labels

all.bdds = list.files(pattern = ".csv")

fromTXT = function(x){
  tmp = fread (file = x) 
  tmp=as.data.table(tmp)
  out = list(tmp)
  return(out)
}

for (x in all.bdds){
  n0 = gsub(".csv", "", x)
  tmp = fromTXT(x)
  assign(n0,tmp[[1]]) 
} 

##Base poblacion2010
load("poblacion2010.RData")
poblacion2010<-as.data.table(dt)


#Funciones
SimpleTab<-function(dt, ValLab, VarLab, var1){
  t1<-dt[,.(freq=.N) ,by=.(dt[[var1]])][, `:=` (prop = (freq/sum(freq)))] %>% adorn_totals(dt, where="row")
  t2<-ValLab[Var==var1, ]
  t2<-t2[, value:=as.character(t2$value)]
  t3<-merge(t1, t2, by.x="dt", by.y="value", all.x = TRUE)
  t3<-t3[, .(codigo, freq, prop)]
  t3<-t3[is.na(codigo), codigo:="Total"]
  t4<-VarLab[Variable==var1]
  t4<-t4[, VarLabel]
  setnames(t3, names(t3), c(t4, "Num", "%"))
}

SimpleTabc<-function(dt, ValLab, VarLab, var1, censo){
  t1<-dt[,.(freq=.N) ,by=.(dt[[var1]])][, `:=` (prop = (freq/sum(freq)))] %>% adorn_totals(dt, where="row")
  t2<-ValLab[Var==var1, ]
  t2<-t2[, value:=as.character(t2$value)]
  t3<-merge(t1, t2, by.x="dt", by.y="value", all.x = TRUE)
  t3<-t3[, .(codigo, freq, prop)]
  t3<-t3[is.na(codigo), codigo:="Total"]
  t4<-VarLab[Variable==var1]
  t4<-t4[, VarLabel]
  setnames(t3, names(t3), c(t4, "Num", "%"))
  t3[,Censo:=as.numeric(censo)]
}


#poblacion2021<-poblacion2021[P12C!="NA",] 
#t1<-poblacion2021[,.(freq=.N) ,by=P12C][, `:=` (prop = (freq/sum(freq)))] %>% adorn_totals(dt, where="row")


CrossTab<-function(dt, ValLab, VarLab, var1, var2){
  t1<-dt[, .(Num=.N) , by=.(dt[[var1]], dt[[var2]])]
  t1<-t1[, `:=` ("%" = Num/sum(Num)), by=.(dt.1)]
  t2<-ValLab[Var==var1]
  t3<-ValLab[Var==var2]
  t4<-merge(t1, t2, by.x="dt", by.y="value", all.x = TRUE)
  t5<-merge(t4, t3, by.x="dt.1", by.y="value", all.x = TRUE)
  t5<-t5[, .(codigo.x, codigo.y,  Num, `%`)]
  t5<-t5[(is.na(codigo.x) & is.na(codigo.y)), `:=` (codigo.x="Total",
                                                    codigo.y="Total")]
  t7<-dcast.data.table(t5, codigo.x ~ codigo.y, value.var = c("Num", "%"))
  t6<-VarLab[Variable==var1]
  t6<-t6[, VarLabel]
  setnames(t7, "codigo.x", t6)
  adorn_totals(t7, where = "row")
}



CrossTabref<-function(dt, ValLab, VarLab, var1, var2){
  tem <- var1
  t1<-dt[, .(Num=.N) , by=.(dt[[var1]], dt[[var2]])]
  t2<-ValLab[Var==var1]
  t3<-ValLab[Var==var2]
  t4<-merge(t1, t2, by.x="dt", by.y="value", all.x = TRUE)
  t5<-merge(t4, t3, by.x="dt.1", by.y="value", all.x = TRUE)
  t5<-t5[, .(dt,codigo.x, codigo.y,  Num)]
  t6<-VarLab[Variable==var1]
  t6<-t6[, VarLabel]
  setnames(t5, "codigo.x", t6)
  t7<-VarLab[Variable==var2]
  t7<-t7[, VarLabel]
  setnames(t5, "codigo.y", t7)
}


CrossTabc<-function(dt, ValLab, VarLab, var1, var2,censo){
  t1<-dt[, .(Num=.N) , by=.(dt[[var1]], dt[[var2]])]
  t1<-t1[, `:=` ("%" = Num/sum(Num)), by=.(dt.1)]
  t2<-ValLab[Var==var1]
  t3<-ValLab[Var==var2]
  t4<-merge(t1, t2, by.x="dt", by.y="value", all.x = TRUE)
  t5<-merge(t4, t3, by.x="dt.1", by.y="value", all.x = TRUE)
  t5<-t5[, .(codigo.x, codigo.y,  Num, `%`)]
  
  #t5<-t5[(is.na(codigo.x) & is.na(codigo.y)), `:=` (codigo.x="Total",
  #                                                  codigo.y="Total")]
  t7<-dcast.data.table(t5, codigo.x ~ codigo.y, value.var = c("Num", "%"))
  t6<-VarLab[Variable==var1]
  t6<-t6[, VarLabel]
  setnames(t7, "codigo.x", t6)
  t7<-t7%>%adorn_totals(t7, where = "row")
  t7<-t7[,Censo:=as.numeric(censo)]
}


#----------------------------------MORTALIDAD 2021----------------------------------

### Porcentaje de hijos fallecidos de las mujeres de 15 a 49 años

##Totales Hijos nacidos vivos

#crear variable para numero de hijos vivos mujeres y hombres
#mujeres
poblacion2021[, HNVM := NULL]
poblacion2021[P3001>= 0, HNVM := P3001]
poblacion2021[P3001==0 & (P3001A==98 |P3001A==99), HNVM := 99 ]

#hombres
poblacion2021[, HNVH := NULL]
poblacion2021[P3002>= 0, HNVH := P3002]
poblacion2021[P3002==0 & (P3002A==98 |P3002A==99), HNVH := 99 ]

#total
poblacion2021[,THNV := NULL]
poblacion2021[ HNVM!=99 & HNVM!=98 & HNVH!=99 & HNVH!=98, THNV := HNVM + HNVH]

#Totalhijos <- poblacion2021[, sum(THNV, na.rm = T)]

#crear variable para número de hijos vivos actualmente
#mujeres
poblacion2021[, HVAM := NULL]
poblacion2021[P3101>= 0, HVAM := P3101]
poblacion2021[P3101==0 & (P3101A==98 |P3101A==99), HVAM := 99 ]

#hombres
poblacion2021[, HVAH := NULL]
poblacion2021[P3102>= 0, HVAH := P3102]
poblacion2021[P3102==0 & (P3102A==98 |P3102A==99), HNVH := 99 ]

#total
poblacion2021[,THVA := NULL]
poblacion2021[ HVAM!=99 & HVAM!=98 & HVAH!=99 & HVAH!=98, THVA := HVAM+ HVAH]

#crear variable de hijos fallecidos

poblacion2021[,THF := NULL]
poblacion2021[, THF := THNV - THVA]


#obtengo el porcentaje

PHF <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49), sum(THF, na.rm = T)/sum(THNV, na.rm = T)])
setnames(PHF, "V1", "PHF")
PHF [,Censo := "2021"]

#por grupos de edad

poblacion2021[,G_edad  := fcase(P03>=15 & P03<=19,1,
                                P03>=20 & P03 <=24, 2,
                                P03>=25 & P03 <=29, 3,
                                P03>=30 & P03 <=34, 4,
                                P03>=35 & P03 <=39, 5,
                                P03>=40 & P03 <=44, 6,
                                P03>=45 & P03 <=49, 7)]

PHFe <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49), sum(THF, na.rm = T)/sum(THNV, na.rm = T), by = .(G_edad)])
setnames(PHFe, "V1", "PHFe")
PHFe[,Censo:= "2021"]

#Versión utilizando las variables de la base directamente

TH <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49) & P3003!=99 & P3103!=99  & P3003!=98 & P3103!=98,c(sum((P3003), na.rm = T),sum(P3103, na.rm = T))])

TH<- TH[,vm:=c("THNV","THVA")]
TH<- TH[,Censo:=c(2021,2021)]
TH =  dcast(TH, Censo  ~  vm, value.var = "V1")

#Obtengo porcentaje
PHF <- TH[, PHF:=((THNV - THVA)/THNV)]


#por grupos de edad

#poblacion2021[,G_edad  := fcase(P03>=15 & P03<=19,1,
#                               P03>=20 & P03 <=24, 2,
#                              P03>=25 & P03 <=29, 3,
#                             P03>=30 & P03 <=34, 4,
#                            P03>=35 & P03 <=39, 5,
#                           P03>=40 & P03 <=44, 6,
#                          P03>=45 & P03 <=49, 7)]

PHFe <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49) &  P3003!=99 & P3103!=99  & P3003!=98 & P3103!=98, sum((THNV - THVA), na.rm = T)/sum(THNV, na.rm = T), by = .(G_edad)])

PHFe=merge(PHFe,PoblacionValLabel_21[Var=="G_edad",], by.x ="G_edad", by.y ="value")
PHFe[,Var:=NULL]
PHFe[,Censo:= "2021"]
setnames(PHFe, "V1", "PHFe")
setnames(PHFe, "G_edad", "cod_var")
setnames(PHFe, "codigo", "G_edad")

#Versión utilizando las variables de la base directamente


THNVe <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49) & P3003!=99 & P3103!=99  & P3003!=98 & P3103!=98, c(sum((P3003), na.rm = T)), by = .(G_edad)])

setnames(THNVe, "V1", "THNVe")

THVAe <- as.data.table(poblacion2021[P02==2 & P03 %between%c(15,49) & P3003!=99 & P3103!=99  & P3003!=98 & P3103!=98, c(sum((P3103), na.rm = T)), by = .(G_edad)])
setnames(THVAe, "V1", "THVAe")


#Obtengo porcentaje
HFe = merge(THNVe,THVAe, all = TRUE)

HFe[, HF:= NULL]
HFe[, HF:= THNVe - THVAe]

PHFe = HFe[,PHFe:=(sum(HF, na.rm = T)/sum(THNVe, na.rm =T)), by = .(G_edad)]

PHFe=merge(PHFe,PoblacionValLabel_21[Var=="G_edad",], by.x ="G_edad", by.y ="value")
PHFe[,Var:=NULL]
PHFe[,Censo:= "2021"]
setnames(PHFe, "G_edad", "cod_var")
setnames(PHFe, "codigo", "G_edad")

###Porcentaje de mujeres de entre 10 a 50 años que murieron estando embarazadas

PME = SimpleTab(Mortalidad2021[M04 %between% c(10,50) & M05 == 2,], MortalidadValLabel_21, MortalidadVarLabel_21, "M05") 
PMEp = SimpleTab(Mortalidad2021Prueba[M04 %between% c(10,50) & M05 == 2,], MortalidadValLabel_21, MortalidadVarLabel_21, "M05") 
PMp = SimpleTab(Mortalidad2021Prueba[M02 == 2 & M04 %between% c(10,50),], MortalidadValLabel_21, MortalidadVarLabel_21, "M05") 


#Sin función
TME <- as.data.table(Mortalidad2021Prueba[M02 == 2 & M04 %between% c(10,50) & M05 == 2, .N])
setnames(TME, "V1", "TME")
TMM <- as.data.table(Mortalidad2021Prueba[M02 ==2 & M04 %between% c(10,50), .N ])
setnames(TMM, "V1", "TMM")
                                           
PMEp <- merge(TME, TMM)
### Porcentaje de personas fallecidas según causa de muerte

PFC = SimpleTab(Mortalidad2021, MortalidadValLabel_21, MortalidadVarLabel_21, "M05")



#-------------------------------------EMIGRACIÓN 2021--------------------------------------------

### Porcentaje de emigrantes que residen en el exterior de acuerdo al país de residencia actual y año de salida

#CON NO RESPONDE
PEE = CrossTabc(Emigracion2021, EmigracionValLabel_21, EmigracionVarLabel_21, "E04C", "E03", 2021)

#SIN NO RESPONDE 
PEEs = CrossTabc(Emigracion2021[E03 %between% c(2010, 2022),], EmigracionValLabel_21, EmigracionVarLabel_21, "E04C", "E03", 2021)

#Porcentaje de emigrantes que residen en el exterior de acuerdo al país de residencia actual

PEPA = SimpleTabc(Emigracion2021, EmigracionValLabel_21, EmigracionVarLabel_21, "E04C", 2021)

#Porcentaje de emigrantes que residen en el exterior de acuerdo al año de salida

PEAS = SimpleTabc(Emigracion2021, EmigracionValLabel_21, EmigracionVarLabel_21, "E03", 2021)


### Porcentaje de emigrantes por año de salida y sexo

NEAS = CrossTabc(Emigracion2021, EmigracionValLabel_21, EmigracionVarLabel_21, "E01", "E03", 2021)


### Porcentaje de inmigrantes extranjeros según país de nacimiento

PIEN = SimpleTabc(poblacion2021[P08 == 3,]
                  , PoblacionValLabel_21, PoblacionVarLabel_21, "P08C", 2021)

table(poblacion2021$P0804)

PIEn <- as.data.table(poblacion2021[P08 == 3, .N / sum(P08, na.rm = T), by = .(P08C_O)])

table(poblacion2021[P08 == 3]$P08C_O)



table(poblacion2021[P08 == 3]$P0804)


#------------------------------------------MORTALIDAD 2010---------------------------------------

### Porcentaje de hijos fallecidos de las mujeres de 15 a 49 años

#Número de hijos nacidos vivos

THNV2010 <- as.data.table(poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P36!=99 & P36!=98,sum(P36,na.rm =T)])
setnames(THNV2010, "V1", "THNV2010")

#Número de hijos vivos actualmente

THVA2010 <- as.data.table(poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P37!=99 & P37!=98,sum(P37,na.rm =T)])
setnames(THVA2010, "V1", "THVA2010")


#Obtengo porcentaje
PHF2010 <- (THNV2010 - THVA2010)/THNV2010
setnames(PHF2010, "V1", "PHF2010")
PHF2010[,Censo:= "2010"]

#por grupos de edad

poblacion2010[,G_edad  := fcase(P03>=15 & P03<=19,1,
                                P03>=20 & P03 <=24, 2,
                                P03>=25 & P03 <=29, 3,
                                P03>=30 & P03 <=34, 4,
                                P03>=35 & P03 <=39, 5,
                                P03>=40 & P03 <=44, 6,
                                P03>=45 & P03 <=49, 7)]

poblacion2010[, HF:=NULL]
poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P37!=99 & P37!=98, HF:= P36-P37]

poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P37!=99 & P37!=98, HF:= P36-P37]

#Número de hijos nacidos vivos

THNV2010e <- as.data.table(poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P36!=99 & P36!=98,sum(P36,na.rm =T), by = .(G_edad)])
setnames(THNV2010e, "V1", "THNV2010e")

THNV2010a <- as.data.table(poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P36!=99 & P36!=98,sum(P36,na.rm =T), by = .(URP)])
setnames(THNV2010e, "V1", "THNV2010e")

#Número de hijos vivos actualmente

THVA2010e <- as.data.table(poblacion2010[I01=="14" & I02=="03"& P01 == 2 & P03 %between%c(15,49) & P37!=99 & P37!=98,sum(P37,na.rm =T), by = .(G_edad)])
setnames(THVA2010e, "V1", "THVA2010e")

#Obtengo porcentaje
HF2010e = merge(THVA2010e, THNV2010e, all = TRUE)

HF2010e[, HF:= NULL]
HF2010e[, HF:= THNV2010e - THVA2010e]

PHF2010e = HF2010e[,sum(HF, na.rm = T)/sum(THNV2010e, na.rm =T), by = .(G_edad)]
setnames(PHF2010e, "V1", "PHF2010e")
PHF2010e[,Censo:= "2010"]


table(EmigracionValLabel_2010$value)
#----------------------------------------------EMIGRACION2010--------------------------------------------

### Porcentaje de emigrantes que residen en el exterior de acuerdo al país de residencia actual y año de salida

#CON NO RESPONDE
PEE2010 = CrossTabc(emigracion2010[I01=="14" & I02=="3",], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M33", "M34", 2010)

#SIN NO RESPONDE 
PEEs = CrossTabc(emigracion2010[I01=="14" & I02=="3"& M33 %between% c(2001, 2010),], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M33", "M34", 2010)
PEES2010 = CrossTabc(emigracion2010[I01=="14" & I02=="3"& M33 %between% c(2001, 2010),], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M34", "M33", 2010)

#Porcentaje de emigrantes que residen en el exterior de acuerdo al país de residencia actual

PEPA2010 = SimpleTabc(emigracion2010, EmigracionValLabel_2010, EmigracionVarLabel_2010, "M34", 2010)

#Porcentaje de emigrantes que residen en el exterior de acuerdo al año de salida

PEAS2010 = SimpleTabc(emigracion2010[I01=="14" & I02=="3",], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M33", 2010)

### Porcentaje de emigrantes por año de salida y sexo

NEAS2010 = CrossTabc(emigracion2010[I01=="14" & I02=="3",], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M33", "M31", 2010)
NEAS2010 = CrossTabc(emigracion2010[I01=="14" & I02=="3",], EmigracionValLabel_2010, EmigracionVarLabel_2010, "M31", "M33", 2010)

### Porcentaje de inmigrantes por país de nacimiento

PIEN2010 = SimpleTabc(poblacion2010[I01=="14" & I02=="3"& P11L ==3,], 
                      PoblacionValLabel_2010, PoblacionVarLabel_2010, "P11", 2010)

##UNIR TABLAS

tab1=rbindlist(list(PHF2010e, PHFe), use.names=FALSE)
tab1_v=setcolorder(tab1, "Censo")

tab2=rbindlist(list(PHF2010e, PHFe), use.names=FALSE)michelle95
write.xlsx(tab1_v, "Hijos fallecidos.xlsx")
write.xlsx(NEAS, "Emigracion_Sexo_país.xlsx")
write.xlsx(NEAS2010, "Emigracion_Sexo_país2010.xlsx")
write.xlsx(PEEs,"Emigracion_país_año.xlsx")
write.xlsx(PEES2010,"Emigracion_país_año2010.xlsx")


#gráfico: Porcentaje de inmigrantes según país de nacimiento


# Gráfico: Porcentaje de hijos fallecidos 

PHF2010e= PHF2010e[,THNVe:=as.double(THNVe)]
PHF2010e= PHF2010e[,THVAe :=as.double(THVAe)]

tabM1=rbindlist(list(PHF2010e,PHFe), use.names=FALSE)

# Gráfico: Porcentaje de hijos fallecidos por grupos de edad de la madre

tabM2=rbindlist(list(PHF2010,PHF), use.names=FALSE)
#Gráficos

g_phf= hchart(tab1_v, "line", hcaes(x = 'G_edad', y = PHFe, group = Censo))%>%
  hc_colors(c("#6E8CB2", "#F1C45F")) %>%
  hc_title(text = "<span style = 'color:#666666'><b>Porcentaje de hijos/as fallecidos de mujeres de 15 a 49 años, por grupos de edad</b></span>", style=list(useHTML=TRUE, fontFamily="Century Gothic", fontSize=13)) %>%
  hc_xAxis(title=list(text="")) %>%
  hc_yAxis(gridLineColor= "none",
           title=list(text=""),
           labels=list(enabled=FALSE)) %>%
  hc_plotOptions(line = list(
    dataLabels= list(
      align = "center",
      enabled = TRUE,
      format='{point.y}%',
      style=list(textOutline="none"))))%>%
  hc_legend(
    align = "left",
    verticalAlign = "top"
  )
g_phf

#gráfico Porcentaje de personas fallecidos/as por causa de muerte


Tab1f=PFC[,`%` := round((100*`%`),1)]
setnames(Tab1f,"Causa del fallecimiento", "A")

g_pfc

g_pfc = hchart(Tab1f[A!="Total",], "treemap", hcaes(x = 'A', value = '`%`'))%>%
  hc_colors(c("#D16B78", "#E4B242", "#7D858B", "#9FBEE5"))%>%
  hc_colorAxis(dataClasses = color_classes(Tab1f[A!="Total",A ]))%>%
  hc_title(text = "<span style = 'color:#666666'><b>Porcentaje de personas fallecidas según causa de muerte</b></span>", style=list(useHTML=TRUE, fontFamily="Century Gothic", fontSize=13))%>%
  hc_plotOptions(treemap = list(
    colorByPoint = TRUE,
    dataLabels= list(
      align = "center",
      enabled = TRUE,
      format='{point.y}%',
      style=list(textOutline="none"))))%>%
  hc_legend(
    align = "left",
    verticalAlign = "top",
    layout = "vertical")
  
  



